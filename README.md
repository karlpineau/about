# À propos de Karl Pineau

- **Nom :** Karl Pineau
- **Situation professionnelle :** Directeur du Media Design lab de [L'École de Design Nantes Atlantique](lecolededesign.com/)
- **Situation professionnelle bis :** Co-président de l'association [Les Designers Ethiques](https://designersethiques.org)
- **Affiliation universitaire :** Equipe EPIN, [Laboratoire Costech](http://www.costech.utc.fr/), Université de Technologie de Compiègne

# Photographie :
[Ma photo](https://framagit.org/karlpineau/about/blob/master/karl.jpg)

# Description :
Karl Pineau est enseignant-chercheur en sciences de l'information et de la communication. Il est directeur du Media Design lab de [L'École de Design Nantes Atlantique](https://www.lecolededesign.com/). Par ailleurs, il est co-président [des Designers Éthiques](https://designersethiques.org), association de recherche-action sur le numérique et les pratiques de design.

Karl Pineau a réalisé son doctorat en sciences de l'information et de la communication au laboratoire Costech de l'Université de technologie de Compiègne. Sa thèse est intitulée "_Le dossier d’œuvre et les musées. Un dossier pour grammatiser les œuvres_". Elle propose une étude du dossier d'œuvre en tant qu'objet de documentation des œuvres d'art à travers une approche informationnelle et ontologique du dossier. La thèse a été réalisée en contrat CIFRE chez un éditeur de logiciel de gestion de collection - [Decalog](https://flora.decalog.net/). Karl Pineau traite l'enjeu de la numérisation du dossier au sein des logiciels de gestion de collection et plus spécifiquement le rôle de grammatisation que recouvre l'œuvre et que le numérique retranscrit difficilement.

À L'École de design Nantes Atlantique, Karl Pineau dirige [le Media design lab](https://www.lecolededesign.com/recherche-et-design-labs/media-design-lab/). Ce dernier explore les enjeux de l’information, de la connaissance et de la communication. Le Lab promeut le design de narrations engageantes et engagées dans les champs de l’information (presse écrite et en ligne, médias, sciences de l’information, systèmes complexes), la médiation (scientifique, événementielle et culturelle) et le social (médias sociaux numériques, lieux porteurs de liens sociaux). En 2021, l’école a choisi d’intégrer le Media Design dans ses programmes pour faire émerger de nouvelles formes d’engagement face aux grands défis actuels de notre monde. À une époque où l’infobésité et les contenus de moindre qualité prédominent, le media designer joue un rôle crucial. En produisant un message construit en lien avec les usagers, le designer contribue à alimenter le débat démocratique et la construction culturelle contemporaine. 

Au sein de L'École de design Nantes Atlantique, Karl Pineau co-anime également l'équipe de recherche de l'école, autour de l'animation [d'un séminaire de recherche](https://www.lecolededesign.com/recherche-et-design-labs/la-recherche-en-design/seminaire-de-recherche/) et [d'un séminaire des doctorants](https://www.lecolededesign.com/recherche-et-design-labs/la-recherche-en-design/seminaire-doctorants/).

Au sein de l'association Designers Ethiques, Karl Pineau co-dirige [le programme de recherche-action autour du design persuasif](https://attention.designersethiques.org/) ainsi que le comité de pilotage de [l'événement Ethics by design](https://ethicsbydesign.fr/). Il est également co-animateur du [podcast Questions d'Asso](https://www.questions-asso.com/), dédié aux enjeux de la vie associative.

# Sur les réseaux sociaux :
- Mastodon : [@karl@masotodon.social](https://mastodon.design/@karl)
- LinkedIn : [KarlPineau](https://www.linkedin.com/in/karlpineau)
- Twitter : [@KarlPineau](https://twitter.com/KarlPineau) (plus vraiment d'actualité)

