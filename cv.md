# Curriculum Vitae - Karl Pineau - Version Académique
(à jour du 13 février 2020)

## Sujets de recherche
- Sciences de l'Information et de la Communication :
    - Sciences de l'Information et de la Documentation :
        - **Modélisation ontologique des données culturelles**
    - Conception de services numériques :
        - **Enjeux éthiques et responsabilité de la conception numérique**

## Expériences professionnelles
- 2018-en cours : **Ingénieur de recherche**, [Decalog](http://decalog.net)
    - En charge des projets d'innovation liés à l'activité Musées et Recherche de Decalog
    - En charge de la gestion du Crédit Impôt Recherche et du Crédit Impôt Innovation pour la partie Musées
    - Veille & prospective dans les domaines de l'histoire de l'art, humanités numériques et musées
- 2016-en cours : **Co-président**, [Collectif les Designers Ethiques](http://designersethiques.org)
- 2017-2018 : **Ingénieur d'études**, Université de Cergy-Pontoise / Archives Nationales
    - En charge de la conception et du développement de la plate-forme collaborative [Testaments de Poilus](https://testaments-de-poilus.huma-num.fr) 
- 2016-2017 : [Stage de fin d'études] **Ingénieur R&D**, Service R&D, [Fondation Europeana](https://europeana.eu)
- 2015 : [Stage] **Chef de projet DSI Musée**, Cité de l'Architecture et du patrimoine
    - Conception et réalisations du site https://collections.citedelarchitecture.fr/

## Formation :
### Formation intiale :
- 2018-en cours : **Doctorat en sciences de l'information et de la communication**, [Laboratoire Costech](costech.utc.fr/), Université de Technologie de Compiègne, sous la direction de Bruno Bachimont et Serge Bouchardon
- 2015-2017 : **Master en sciences de l'information et de la communication**, Ecole Normale Supérieure de Lyon, sous la direction de Jean-Michel Salaün et Benoit Habert
    - Mémoire de recherche : *The recommendation of cultural heritage objects in Europeana Collections*, sous la direction de Pierre-Antoine Champin (Lyon 1) et Antoine Isaac (Europeana) - https://drive.google.com/drive/folders/0B-rjT5C8N2CnX1N1X0psRDJyeFE
- 2010-2015 : **Premier cycle d'histoire de l'art**, Ecole du Louvre - Spécialité histoire de l'architecture
- 2009-2010 : L1 Histoire de l'Art et Archéologie, Université de Nantes
- 2006-2009 : Lycée Gabriel Guist'hau, Nantes - Bac L, section histoire des arts

### Formation continue :
- 2020/06/25 : **Entrepreneuriat et innovation**, UTC, 3h
- 2020/06/25 : **Se rendre employable**, UTC, 3h
- 2020/06/08-08 : **Piloter un projet : conduire, animer, gérer**, Sorbonne Université, 14h
- 2020/04/24 : **Comprendre le processus de recrutement du point de vue du recruteur**, Sorbonne Université, 2h
- 2020/04/22-28 : **Travaillez mieux en équipe et apprenez à manager**, Sorbonne Université, 14h
- 2020/01/27-29 : **Séminaire GE90: Les chemins de la transition : écologie, économie et société**, UTC, 25h 
- 2020/01/22 : **Relations grands groupes - start-up**, ANRT, 7h
- 2020/01/14 : **Propriétés intellectuelle & industrielle**, ANRT / INPI, 7h
- 2019/11/21 : **Une thèse en SIC en CIFRE : valorisation et insertion des jeunes docteurs**, SFSIC / ANRT, 7h
- 2019/06/21 : **Propriété industrielle, brevets, veille**, UTC, 12h
- 2019/01/21-24 : **Séminaire Phiteco**, UTC, 25h

## Activités
### Publications
- 2019, H2PTM 19 - **Définition d'un cycle de vie des biens culturels, un enjeu pour l'industrie de l'ingénierie des connaissances**, Karl Pineau, Bruno Bachimont, Serge Bouchardon
- 2019, RODBH 19 - **Definition of the Life Cycle of Cultural Property, the Concept of Stratum**, Karl Pineau (http://ceur-ws.org/Vol-2532/paper5.pdf)

### Projets de recherche
- 2020, [Matrice collaborative d’analyse de la persuasion des systèmes persuasif numériques](https://docs.google.com/spreadsheets/d/1dj_KIKzyPlSND1-fSYNb0BgupCmh2XoYTPkFTKPiaq0/edit#gid=1991215211) - Projet de développement de plugin en cours
- 2018-2020, [Coordination du programme de recherche Captologie & Design persuasif des Designers Ethiques](https://attention.designersethiques.org)
- 2018-2020, [Participation à l'écriture de l'ouvrage collaboratif Design éthique numériques des Designers Ethiques](https://livre-ethique-numerique.designersethiques.org/)

### Projets de R&D
- 2018-2020, [Projet de captcha pour l'indexation du patrimoine culturel](https://captchan.teklia.com/) en partenariat avec la société Teklia, le Ministère de la culture, les Archives nationales, la BNF et le musée de Bretagne

### Organisation de colloques et d'évènements
#### Evènements d'envergure nationale
- 2020/09/28 - 2020/10/02 - **[Ethics by design 2020](https://2020.ethicsbydesign.fr)**
- 2020/06/18 - **Journée d'études sur l'apport des SIC à l'analyse des plate-formes**
- 2018/10/01 - 2018/10/02 - **[Ethics by design 2018](https://2018.ethicsbydesign.fr)**
- 2017/06/17 - **[Ethics by design 2017](https://2017.ethicsbydesign.fr)**

#### Ateliers et séminaires locaux
- 2020/05/30 - Séminaire Recherche/Action Designers Ethiques - Le design systémique - Intervenant : Patrick Maruéjouls
- 2020/02/08 - Séminaire Recherche/Action Designers Ethiques - Les enjeux de la low tech en design - Intervenant : Gauthier Roussilhe
- 2019/06/11 - Atelier Design Ethique - Rennes - La place de la responsabilité dans la pratique professionnelle
- 2019/02/23 - Atelier Design Ethique n°2 - La responsabilité dans la pratique du design
- 2019/01/12 - Atelier Design Ethique n°1 - Cartographique du design éthique

### Interventions 
*Colloques, journées d'études, évènements...*
- 2020/02/20 - **Les ontologies comme système de représentation de l'information, l'exemple du domaine culturel**, Karl Pineau | (JE) Reticulum (2) Architecture(s) de l’information - Lab. MICA, Université de Bordeaux-Montaigne 
- 2020/02/14 - **Le design de l'attention : d'une prise de conscience à une action individuelle**, Karl Pineau | (Colloque) Guerre de l’attention : un défi éthique pour les communicants ? - Master Conseil éditorial de la Faculté des Lettres de Sorbonne Université
- 2020/02/07 - **Démarches collaboratives d’appropriation et de valorisation du patrimoine**, Karl Pineau, Yves-Armel Martin | (Formation) Patrimoines au XXIème siècle - Cycle des Hautes Etudes de la Culture
- 2020/02/04 - **Politiques de l’intelligence artificielle, éthique et "explicabilité"**, Raja Chatila, Karl Pineau, Clément Hennin | (JE) Transnum - Costech, UTC
- 2020/02/03 - **Ecriture numérique des données culturelles : exploitation des ontologies en contexte muséal**, Karl Pineau | (Séminaire) Séminaire doctoral CELSA / IReMus
- 2020/01/21 - **Téléphones & apps : attention à votre attention**, Karl Pineau | (Formation) Collège Jean-Baptiste Clément, intervention en classe de 5ème à la demande du CD de Seine-Saint-Denis
- 2020/01/09 - **Ecriture numérique des données culturelles**, Karl Pineau | (Séminaire) Séminaire d'équipe EPIN, Costech, UTC
- 2019/12/02 - **Economie & design de l'attention**, Karl Pineau | (Formation) Fab AirFrance
- 2019/10/17 - **Définition d'un cycle de vie des biens culturels, un enjeu pour l'industrie de l'ingénierie des connaissances**, Karl Pineau | (Colloque), H2PTM
- 2019/06/24 - **Définition d'un cycle de vie des biens culturels**, Karl Pineau | (Séminaire) Journée doctorale, Costech, UTC
- 2019/05/25 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Jeudis de l'Innovation, CD de Loire-Atlantique
- 2019/04/12 - **Le design peut-il rendre le citoyen connecté plus « smart » ?**, Karl Pineau (modération), Clément Mabi, Latifa Danfakha, Julie De Brito Ventura, Irénée Regnauld | (Colloque) La cité contributive, Biennale du Design de Saint-Etienne
- 2019/04/10 - **Le projet Captch'AN**, Karl Pineau | (Journée d'études) Fonte Gaia, Université de Grenoble
- 2019/04/05 - **Definition of the Life Cycle of Cultural Property, the Concept of Stratum**, Karl Pineau | (Journée d'études) ROBDH 2019 / Data For History, University of Leipzig
- 2019/03/21 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Grand Barouf du Numérique, Lille Métropole by OuiShare
- 2019/03/15 - **Le projet Captch'AN**, Karl Pineau | (Journée d'études) Archives participatives : d’une logique de guichet à une logique de co-construction, Master 2 Gestion des Archives et de l’Archivage, université de Versailles Saint-Quentin-en-Yvelines
- 2019/02/22 - **Pour une approche éthique du design**, Karl Pineau | (Intervention) Sommet Mondial du Design, Paris
- 2019/02/01 - **Approches du design éthique**, Karl Pineau | (Formation) Fab Design, SNCF
- 2019/01/17 - **La forme des choix - Données personnelles, design et frictions désirables**, Karl Pineau, Celia Hodent, Hubert Guillaud, Corinne Moreau, Marie Potel | (Table-ronde) LINC, CNIL
- 2019/01/11 - **Numérique & éthique** (table-ronde) | Forum numérique IXAD, Lille
- 2019/01/08 - **Algorithmes, éthique et journalisme**, Karl Pineau, Irénée Regnauld | (Formation) M2 Journalisme, Université Nanterre
- 2018/11/29 - **Meetup API Days - Table-ronde Conception et responsabilité**, Mehdi Medjaoui, Isabelle Reusa, Karl Pineau | (Intervention) Le Laptop
- 2018/11/27 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Petit Dej' Ferpection
- 2018/11/27 - **Meetup Design Ethique**, Henning Fritzenwalder, François-Xavier Ferrari, Karl Pineau | (Intervention) Meetup Axance
- 2018/11/20 - **Méthode de diagnostique du design de l'attention**, Karl Pineau, Jérémie Poiroux | (Intervention) Meetup Flupa Bruxelles
- 2018/10/26 - **Articulation entre les données catalographique et la recherche en histoire de l’art, un enjeu encore à traiter**, Karl Pineau, Bruno Bachimont | (Colloque) CRIHN, Université de Montréal
- 2018/09/20 - **Conclusion de la journée "Le Numérique : en Confiance ?"**, Karl Pineau | (Journée d'études) Costech, UTC
- 2018/09/18 - **Méthode de diagnostique du design de l'attention**, Lénaïc Faure, Karl Pineau | (Intervention) Meetup Flupa Paris
- 2018/07/12 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Jeudis Fabernovel
- 2018/07/09 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Aktan
- 2018/06/13 - **Table-ronde "ordre ancien et nouveau désordre dans les collections"**, Sophier Bertrand, Florent Aziosmanoff, Albertine Meunier, Joachim Montessuis, Karl Pineau | (Colloque) Vertigo, IRCAM
- 2018/06/07 - **Geek et digital addict : Votre attention s'il vous plait !**, Tristan Nitot, Karl Pineau, Yves Citton, Camille Diao | (Intervention) Maif Social Club
- 2018/04/12 - **Economie & design de l'attention**, Karl Pineau, Jérémie Poiroux | (Séminaire) ENSCI Les Ateliers / Fing, Rétro-Design de l'Attention
- 2018/04/09 - **Economie & design de l'attention**, Karl Pineau | (Séminaire) ENSCI Les Ateliers, Design en Séminaires
- 2018/03/28 - **Economie & design de l'attention**, Karl Pineau | (Intervention) ECV Digital Bordeaux
- 2018/03/23 - **Cartographie des pathologies de l'attention**, Jérémie Poiroux, Karl Pineau | (Atelier-Commission) Grand Barouf du Numérique, Lille
- 2018/03/19 - **Economie & design de l'attention**, Karl Pineau | (Intervention) Association OpenLaw
- 2018/02/21 - **Projection du documentaire Ethics for Design**, Karl Pineau, Andy Kwok | (Intervention) Cobalt Poitiers
- 2018/01/17 - **Economie de l'attention**, Karl Pineau, Jérémie Poiroux | (Séminaire) ENSSIB / ENS de Lyon
- 2017/12/13 - **Economie & design de l'attention**, Karl Pineau, Jérémie Poiroux | (Intervention) Ca se Disrupte, Orange Consulting
- 2017/10/18 - **Apéro Ethics by design**, Mellie La Roque, Jérémie Poiroux, Karl Pineau | (Intervention) Ecole de Design Nantes Atlantique, Ready Design Lab
- 2017/10/18 - **Transcription collaborative, le projet Testaments de Poilus**, Florence Clavaud, Karl Pineau, Christine Nougaret | (Colloque) Le crowdsourcing, pour partager, enrichir et publier des sources patrimoniales, Université d'Angers

### Charges de cours
#### 2020-2021
- **Culture numérique** - 2A, Collège Universitaire, Sciences Po, 28h - dir. Dominique Cardon
- **Approches responsables de la conception numérique** - 5A, HETIC, 24h
- **Approches stratégiques des médias sociaux** - L3 Info-Comm, Paris XIII, 21h
- **Outils collaboratifs** - L2 Info-Comm, Univ. Paris XIII, 18h
- **Ecriture numérique** - L1 Info-Comm, Univ. Paris XIII, 18h
- **Recherche documentaire** - L2 Info-Comm, Univ. Paris XIII, 12h
- **Ethique & numérique** - M1, Ecole de Design Nantes-Atlantique, 4h

#### 2019-2020
- **Culture numérique** - 2A, Collège Universitaire, Sciences Po, 24h - dir. Dominique Cardon
- **Approches responsables de la conception numérique** - 5A, HETIC, 24h
- **Approches stratégiques des médias sociaux** - L3 Info-Comm, Paris XIII, 21h
- **Outils collaboratifs** - L2 Info-Comm, Univ. Paris XIII, 18h
- **Recherche documentaire** - L2 Info-Comm, Univ. Paris XIII, 12h
- **Ethique & numérique** - M1, Ecole de Design Nantes-Atlantique, 4h

#### 2018-2019
- **Culture numérique** - 2A, Collège Universitaire, Sciences Po, 36h - dir. Dominique Cardon
- **Cyberculture et médias sociaux** - L3 Info-Comm, Paris XIII, 21h
- **Outils collaboratifs** - L2 Info-Comm, Univ. Paris XIII, 18h
- **Recherche documentaire** - L2 Info-Comm, Univ. Paris XIII, 24h
- **Maitrise des données** - M2 PSMIC, Univ. Paris XIII, 12h
- **Ethique & numérique** - 4A, Ecole de Design Nantes-Atlantique, 4h
- **Ethique & numérique** - 4A, HETIC, 12h
- **Law Design** - ERAGE (Ecole des Avocats du Grand-Est), 6h

#### 2017-2018
- **Cyberculture et médias sociaux** - L3 Info-Comm, Paris XIII, 21h
- **Outils collaboratifs** - L2 Info-Comm, Univ. Paris XIII, 9h
- **Recherche documentaire** - L2 Info-Comm, Univ. Paris XIII, 12h
- **Ethique & numérique** - M1, IAE Poitiers, 4h

### Suivi d'étudiants
- 2020, UTC Compiègne : (dir. TX) **Conception d’un prototype de data visualisation du cycle de vie d’une oeuvre d’art**, Leïla Ballouard & Olympe Quillet
- 2020, Ecole de Condé : (tuto.) **Une dérive de l'attention, penser une "affordance hacktiviste"**, Aurélia Fabre, MDGRI Design Graphique
- 2019, Institut Léonard de Vinci, thèse professionnelle : (dir.) **Ethique par le design : peut-ton concevoir des produits digitaux rentables et respectueux de l'utilisateur ?**, Jorge Gonçalves, MBA
- 2019, ECV Digitale Nantes, livre blanc : (tuto.) **L'éthique et le design UX**, Flora Brochier, 5ème année
- 2018, ECV Digitale Bordeaux, livre blanc : (tuto.) **UX et éthique**, Thierry Vilaysith, 5ème année 
- 2018, ENS de Lyon, mémoire de recherche : (dir.) **Évaluer et juger la captation attentionnelle d’un service ou produit sur leurs utilisateurs**, Lénaïc Faure 

### Hackathons & évènements de créativité
- 2018 - 8 & 9 décembre : **[Hackathon des Archives nationales](https://www.hackathon-archives-nationales.org/)** - [Projet CaptchA-N](https://framagit.org/Ash_Crow/captchan/) (coup de coeur innovation)
- 2018 - 24 & 25 novembre : **[3e édition du Hackathon de la BNF](http://www.bnf.fr/fr/evenements_et_culture/conferences/f.hackathon_2018.html)** - [Projet Mix Tes Romans](http://gallicacard.histoiredelart.fr/new/) (lauréat)
- 2018 - 1, 2 & 3 juin : **[Persée Up'](data.persee.fr/perseeup/)**
- 2017 - 25 & 26 novembre : **[2e édition du Hackathon de la BNF](https://gallica.bnf.fr/blog/05122017/retour-sur-le-hackathon-bnf-2017)** - [Projet Musiviz](http://musiviz.histoiredelart.fr/app/#!/)  (lauréat)

### Interviews
(section à compléter)
- https://numerique-investigation.org/vers-un-webdesign-plus-ethique/
- https://nouveauxmedias.fr/karl-pineau-design-ethique/
- https://www.alternatives-economiques.fr/quest-pousse-a-toujours-plus-cliquer/00091397 

### Prix
- 2019 : Lauréat de l'**Appel à services innovants du Ministère de la Culture**
    - Pour le développement d'un POC du [projet Captch'AN](dculturelles.hypotheses.org/33) ;
    - En partenariat avec les sociétés Teklia & Decalog ;
    - Soutenu par la BNF, les Archives nationales et le musée de Bretagne.

